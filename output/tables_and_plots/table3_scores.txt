Testing Yelp-Polarity (pretrained)
██tensor(1.9921)
Testing Yelp-Full (pretrained)
██tensor(29.1240)
Testing AG-News (pretrained)
██tensor(6.)
Testing DBPedia (pretrained)


```text
=== Software === 
python       : 3.7.2
fastai       : 1.0.60
fastprogress : 0.2.2
torch        : 1.4.0
torch cuda   : 10.1 / is **Not available** 

=== Hardware === 
No GPUs available 

=== Environment === 
platform     : Linux-4.4.0-170-generic-x86_64-with-debian-9.8
distro       : Debian GNU/Linux 9 stretch
conda env    : Unknown
python       : /usr/local/bin/python
sys.path     : /input/run_test/verification/pretrained
/usr/local/lib/python37.zip
/usr/local/lib/python3.7
/usr/local/lib/python3.7/lib-dynload
/usr/local/lib/python3.7/site-packages
no supported gpus found on this system
```

Please make sure to include opening/closing ``` when you paste into forums/github to make the reports appear formatted as code sections.

██tensor(0.7057)
