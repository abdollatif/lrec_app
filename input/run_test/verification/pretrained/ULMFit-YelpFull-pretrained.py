#Credits:
#ULMFiT https://github.com/fastai/fastai/blob/master/examples/ULMFit.ipynb

import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="2"
os.environ["FASTAI_HOME"]="/input"









from fastai.text import *
import fastai
import torch



def random_seed(seed_value, use_cuda):
    np.random.seed(seed_value) # cpu vars
    torch.manual_seed(seed_value) # cpu  vars
    random.seed(seed_value) # Python
    if use_cuda: 
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value) # gpu vars
        torch.backends.cudnn.deterministic = True  #needed
        torch.backends.cudnn.benchmark = False
 




random_seed(42, True)

path = untar_data("yelp_full")
bs = 48
data_clas = load_data(path, 'data_clas.pkl', bs=bs)
data_clas_bwd = load_data(path, 'data_clas.pkl', bs=bs, backwards=True)

learn = text_classifier_learner(data_clas, AWD_LSTM, drop_mult=0.5, pretrained=False)
learn.load('yelp_full_fwd_clas')

learn_bwd = text_classifier_learner(data_clas_bwd, AWD_LSTM, drop_mult=0.5, pretrained=False)
learn_bwd.load('yelp_full_bwd_clas')

pred_fwd,lbl_fwd = learn.get_preds(ordered=True)
pred_bwd,lbl_bwd = learn_bwd.get_preds(ordered=True)
final_pred = (pred_fwd+pred_bwd)/2
total_acc = accuracy(final_pred, lbl_fwd)
print(str((1 - total_acc)*100))