المحكمة البريطانية العليا تشترط موافقة البرلمان لإطلاق بريكسيت
 قضت المحكمة العليا في بريطانيا الثلاثاء يناير كانون الثاني بضرورة الحصول على موافقة البرلمان للشروع في عملية خروج البلاد من الاتحاد الأوروبي وبذلك أيدت المحكمة العليا موقف محكمة لندن العليا التي سبق لها أن قررت أنه لا يحق لحكومة البلاد بدء عملية بريكسيت دون استشارة البرلمان أولا وقد يؤدي هذا القرار الذي صوت لصالحه قضاة من أصل في المحكمة العليا إلى تأجيل تنفيذ القرار الذي اتخذه الشعب البريطاني خلال استفتاء يونيو حزيران الماضي على خلفية المناقشات الساخنة في البرلمان بشأن شروط الخروج من الاتفاق وأوضحت المحكمة أنه على الرغم من أن التعديلات العادية على القوانين الخاصة بالمشاركة في اتفاقات دولية لا تتطلب عادة موافقة البرلمان عليها لكن الوضع يعد مختلفا فيما يخص بريكسيت الذي سيؤثر حسب رأي القضاة على وضع حقوق المواطنين البريطانيين وفي الوقت نفسه أكدت المحكمة أنه لا داعي لاستشارة البرلمانات الإقليمية برلمانات اسكتلندا وإيرلندا الشمالية وويلز لبدء عملية الخروج من الاتحاد الأوروبي وعلى الرغم من قرار المحكمة أعلنت الحكومة البريطانية أنها ما زالت تأمل في بدء عملية الخروج في مارس آذار المقبل  اوكسانا شفانديوك