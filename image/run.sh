
pip3 install fastai
pip3 install distro

#
#Testing, verificaiton, pretrained
#

echo "Testing Yelp-Polarity (pretrained)" | tee /output/tables_and_plots/table3_scores.txt
python /input/run_test/verification/pretrained/ULMFit-YelpBinary-pretrained.py | tee -a /output/tables_and_plots/table3_scores.txt

echo "Testing Yelp-Full (pretrained)" | tee -a /output/tables_and_plots/table3_scores.txt
python /input/run_test/verification/pretrained/ULMFit-YelpFull-pretrained.py | tee -a /output/tables_and_plots/table3_scores.txt

echo "Testing AG-News (pretrained)" | tee -a /output/tables_and_plots/table3_scores.txt
python /input/run_test/verification/pretrained/ULMFit-AGNews-pretrained.py | tee -a /output/tables_and_plots/table3_scores.txt

echo "Testing DBPedia (pretrained)" | tee -a /output/tables_and_plots/table3_scores.txt
python /input/run_test/verification/pretrained/ULMFit-DBPedia-pretrained.py | tee -a /output/tables_and_plots/table3_scores.txt

echo "Testing IMDB (pretrained)" | tee /output/tables_and_plots/table2_scores.txt
python /input/run_test/verification/pretrained/ULMFit-IMDB-pretrained.py | tee -a /output/tables_and_plots/table2_scores.txt

echo "Testing TREC6 (pretrained)" | tee -a /output/tables_and_plots/table2_scores.txt
python /input/run_test/verification/pretrained/ULMFit-TREC6-pretrained.py | tee -a /output/tables_and_plots/table2_scores.txt

echo "Testing IMDB (non-pretrained)" | tee /output/tables_and_plots/table4_scores.txt 
python /input/run_test/verification/not-pretrained/ULMFit-IMDB-not-pretrained.py | tee -a /output/tables_and_plots/table4_scores.txt

echo "Testing AGNews (non-pretrained)" | tee -a /output/tables_and_plots/table4_scores.txt
python /input/run_test/verification/not-pretrained/ULMFit-AGNews-not-pretrained.py | tee -a /output/tables_and_plots/table4_scores.txt

echo "Testing TREC6 (non-pretrained)" | tee -a /output/tables_and_plots/table4_scores.txt
python /input/run_test/verification/not-pretrained/ULMFit-TREC6-not-pretrained.py | tee -a /output/tables_and_plots/table4_scores.txt





#
#Training, verification, pretrained
#


#echo "Training Yelp-Polarity (pretrained)"
#rm -rf /input/yelp_binary/train
#rm -rf /input/yelp_binary/test
#mkdir -p /input/yelp_binary/train
#mkdir -p /input/yelp_binary/test
#for i in 1 2
#do
#mkdir /input/yelp_binary/train/$i
#mkdir /input/yelp_binary/test/$i
#done
#python /verification/pretrained/ULMFit-YelpBinary-pretrained.py

#echo "Training Yelp-Full (pretrained)"
#rm -rf /input/yelp_full/train
#rm -rf /input/yelp_full/test
#mkdir -p /input/yelp_full/train
#mkdir -p /input/yelp_full/test
#for i in 1 2 3 4 5
#do
#mkdir /input/yelp_full/train/$i
#mkdir /input/yelp_full/test/$i
#done
#python /verification/pretrained/ULMFit-YelpFull-pretrained.py

#echo "Training DBPedia (pretrained)"
#rm -rf /input/dbpedia/train
#rm -rf /input/dbpedia/test
#mkdir -p /input/dbpedia/train
#mkdir -p /input/dbpedia/test
#for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14
#do
#mkdir /input/dbpedia/train/$i
#mkdir /input/dbpedia/test/$i
#done
#python /verification/pretrained/ULMFit-DBPedia-pretrained.py

#echo "Training TREC6 (pretrained)"
#python /verification/pretrained/ULMFit-TREC6-pretrained.py
#echo "Training TREC6 (non-pretrained)"
#python /verification/not_pretrained/ULMFit-TREC6-not-pretrained.py

#echo "Training AGNews (pretrained)"
#rm -fr /input/agnews/train
#rm -fr /input/agnews/test
#mkdir -p /input/agnews/train
#mkdir -p /input/agnews/test
#for i in 1 2 3 4
#do
#mkdir /input/agnews/train/$i
#mkdir /input/agnews/test/$i
#done
#python /verification/pretrained/ULMFit-AGNews-pretrained.py
#echo "Training AG-News (non-pretrained)"
#python /verification/not_pretrained/ULMFit-AGNews-not-pretrained.py


#echo "Training IMDB (pretrained)"
#python /verification/pretrained/ULMFit-IMDB-pretrained.py
#echo "Training IMDB (non-pretrained)"
#python /verification/not_pretrained/ULMFit-IMDB-not-pretrained.py






