#Credits:
#ULMFiT https://github.com/fastai/fastai/blob/master/examples/ULMFit.ipynb


import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ["FASTAI_HOME"]="/input"


from fastai.text import *
import fastai
import torch


fastai.utils.collect_env.show_install()



def random_seed(seed_value, use_cuda):
    np.random.seed(seed_value) # cpu vars
    torch.manual_seed(seed_value) # cpu  vars
    random.seed(seed_value) # Python
    if use_cuda: 
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value) # gpu vars
        torch.backends.cudnn.deterministic = True  #needed
        torch.backends.cudnn.benchmark = False
        
random_seed(42, True)

bs,bptt=64,70
path_trec6 = untar_data("trec6")
data_lm_trec6 = (TextList.from_folder(path_trec6)
            .filter_by_folder(include=['train', 'test']) 
            .split_by_rand_pct(0.1)
            .label_for_lm()           
            .databunch(bs=bs, bptt=bptt, num_workers=0))
data_lm_trec6.save('data_lm_trec6.pkl')
data_lm_trec6 = load_data(path_trec6, 'data_lm_trec6.pkl', bs=bs, num_workers=0)
#data_lm_trec6.show_batch()
data_lm_trec6_bwd = load_data(path_trec6, 'data_lm_trec6.pkl', bs=bs, bptt=bptt, backwards=True, num_workers=0)
#data_lm_trec6_bwd.show_batch()

learn_trec6 = language_model_learner(data_lm_trec6, AWD_LSTM)
#learn_trec6 = learn_trec6.to_fp16(clip=0.1)# helps avoid underflow with small learning rates
learn_trec6.path = path_trec6
#learn_trec6.lr_find()
#learn_trec6.recorder.plot()
learn_trec6.fit_one_cycle(1, 1e-2, moms=(0.8,0.7), wd=0.1)
learn_trec6.unfreeze()
learn_trec6.fit_one_cycle(15, 2e-3, moms=(0.8,0.7), wd=0.1)#8
learn_trec6.save_encoder('trec6_fwd_enc')

learn_trec6 = language_model_learner(data_lm_trec6_bwd, AWD_LSTM)
#learn_trec6 = learn_trec6.to_fp16(clip=0.1)
learn_trec6.path = path_trec6
learn_trec6.fit_one_cycle(1, 1e-2, moms=(0.8,0.7), wd=0.1)
learn_trec6.unfreeze()
learn_trec6.fit_one_cycle(15, 2e-3, moms=(0.8,0.7), wd=0.1)
learn_trec6.save_encoder('trec6_bwd_enc')



path = untar_data("trec6")
bs = 64
data_clas = (TextList.from_folder(path, vocab=data_lm_trec6.vocab)
            .filter_by_folder(include=['train', 'test']) 
            .split_by_folder(valid='test')
            .label_from_folder(classes=['ABBR', 'DESC', 'ENTY', 'HUM', 'LOC', 'NUM'])          
            .databunch(bs=bs, num_workers=0))
data_clas.save('data_clas.pkl')
data_clas = load_data(path, 'data_clas.pkl', bs=bs, num_workers=0)
#data_clas.show_batch()
data_clas_bwd = load_data(path, 'data_clas.pkl', bs=bs, backwards=True, num_workers=0)
#data_clas_bwd.show_batch()



learn = text_classifier_learner(data_clas, AWD_LSTM, drop_mult=0.5, pretrained=False)
learn.path = path
learn.load_encoder('trec6_fwd_enc')
lr = 1e-1
learn.fit_one_cycle(1, lr, moms=(0.8,0.7), wd=0.1)
learn.freeze_to(-2)
lr /= 2
learn.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn.freeze_to(-3)
lr /= 2
learn.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn.unfreeze()
lr /= 5
learn.fit_one_cycle(50, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn.save('trec6_fwd_clas')



learn_bwd = text_classifier_learner(data_clas_bwd, AWD_LSTM, drop_mult=0.5, pretrained=False)
learn_bwd.path = path
learn_bwd.load_encoder('trec6_bwd_enc')
learn_bwd.fit_one_cycle(1, lr, moms=(0.8,0.7), wd=0.1)
learn_bwd.freeze_to(-2)
lr /= 2
learn_bwd.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn_bwd.freeze_to(-3)
lr /= 2
learn_bwd.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn_bwd.unfreeze()
lr /= 5
learn_bwd.fit_one_cycle(50, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn_bwd.save('trec6_bwd_clas')





pred_fwd,lbl_fwd = learn.get_preds(ordered=True)
fwd_acc = accuracy(pred_fwd, lbl_fwd)
pred_bwd,lbl_bwd = learn_bwd.get_preds(ordered=True)
bwd_acc = accuracy(pred_bwd, lbl_bwd)
final_pred = (pred_fwd+pred_bwd)/2#bi-directionality is similar to the idea of the multi-head attention
total_acc = accuracy(final_pred, lbl_fwd)
