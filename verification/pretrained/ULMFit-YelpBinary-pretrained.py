#Credits:
#ULMFiT https://github.com/fastai/fastai/blob/master/examples/ULMFit.ipynb





import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"
os.environ["FASTAI_HOME"]="/input"


from fastai.text import *
import fastai
import torch




def random_seed(seed_value, use_cuda):
    np.random.seed(seed_value) # cpu vars
    torch.manual_seed(seed_value) # cpu  vars
    random.seed(seed_value) # Python
    if use_cuda: 
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value) # gpu vars
        torch.backends.cudnn.deterministic = True  #needed
        torch.backends.cudnn.benchmark = False



def format_data(data='train'):

  config = Config()
  count=0
  with open(str(config.data_path()) + '/yelp_binary/' + data + '.csv', 'r') as f:
      reader = csv.reader(f)
      next(reader)
      for row in reader:
          #print(row[0] + "=======>" + row[1])
          text_file = open(str(config.data_path()) + '/yelp_binary/' + data + "/" + row[0] + "/" + str(count) + ".txt", "wt")
          n = text_file.write(row[1])
          text_file.close()
          count = count + 1







fastai.utils.collect_env.show_install()

print('Processing train')
format_data('train')
print('Processing test')
format_data('test')
print('Done re-formatting data')



random_seed(42, True)


bs,bptt=100,80
path = untar_data("yelp_binary")
data_lm = (TextList.from_folder(path)
            .filter_by_folder(include=['train', 'test']) 
            .split_by_rand_pct(0.1)
            .label_for_lm()           
            .databunch(bs=bs, bptt=bptt, num_workers=0))
data_lm.save('data_lm.pkl')
data_lm = load_data(path, 'data_lm.pkl', bs=bs, bptt=bptt, num_workers=0)
data_bwd = load_data(path, 'data_lm.pkl', bs=bs, bptt=bptt, backwards=True)

learn = language_model_learner(data_lm, AWD_LSTM)
learn.fit_one_cycle(1, 2e-2, moms=(0.8,0.7), wd=0.1)
learn.unfreeze()

learn.fit_one_cycle(15, 2e-3, moms=(0.8,0.7), wd=0.1)
learn.save_encoder('yelp_polarity_fwd_enc')

learn.fit_one_cycle(1, 2e-2, moms=(0.8,0.7), wd=0.1)
learn.unfreeze()
learn.fit_one_cycle(15, 2e-3, moms=(0.8,0.7), wd=0.1)
learn.save_encoder('yelp_polarity_bwd_enc')

path = untar_data("yelp_binary")
bs = 48
data_clas = (TextList.from_folder(path, vocab=data_lm.vocab)
             .split_by_folder(valid='test')
             .label_from_folder(classes=['1', '2'])
             .databunch(bs=bs, num_workers=0))
data_clas.save('data_clas.pkl')
data_clas = load_data(path, 'data_clas.pkl', bs=bs)
data_clas_bwd = load_data(path, 'data_clas.pkl', bs=bs, backwards=True)

learn = text_classifier_learner(data_clas, AWD_LSTM, drop_mult=0.5, pretrained=False)
learn.load_encoder('yelp_polarity_fwd_enc')

lr = 1e-1
learn.fit_one_cycle(1, lr, moms=(0.8,0.7), wd=0.1)
learn.freeze_to(-2)
lr /= 2
learn.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn.freeze_to(-3)
lr /= 2
learn.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn.unfreeze()
lr /= 5
learn.fit_one_cycle(2, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn.save('yelp_polarity_fwd_clas')
learn.load('yelp_polarity_fwd_clas')

learn_bwd = text_classifier_learner(data_clas_bwd, AWD_LSTM, drop_mult=0.5, pretrained=False)
learn_bwd.load_encoder('yelp_polarity_bwd_enc')
learn_bwd.freeze_to(-2)
lr /= 2
learn_bwd.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn_bwd.freeze_to(-3)
lr /= 2
learn_bwd.fit_one_cycle(1, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn_bwd.save("yelp_polarity_bwd_third")
learn_bwd.load("yelp_polarity_bwd_third")
learn_bwd.unfreeze()
lr /= 5
learn_bwd.fit_one_cycle(2, slice(lr/(2.6**4),lr), moms=(0.8,0.7), wd=0.1)
learn_bwd.save('yelp_polarity_bwd_clas')
learn_bwd.load('yelp_polarity_bwd_clas')

pred_fwd,lbl_fwd = learn.get_preds(ordered=True)
pred_bwd,lbl_bwd = learn_bwd.get_preds(ordered=True)

final_pred = (pred_fwd+pred_bwd)/2
accuracy(final_pred, lbl_fwd)